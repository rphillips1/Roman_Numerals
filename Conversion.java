
/**
 * Class for converstion of a given year to its Roman numeral equivalent
 * 
 * @author Rick W. Phillips
 * @version June 16, 2016
 */
public class Conversion
{
    public static String convert(int year) {
        int thousand = year / 1000; // Get the thousands digit
        int hundred = (year % 1000) / 100; // Get the hundreds digit
        int ten = (year % 100) / 10; // Get the tens digit
        int one = year % 10; // Get the ones digit
        String value = ""; // Initialize the return String
        
        // Limited version of the thousands additions. This could be 
        // expanded to handle larger numbers.
        if(thousand != 0) {
            for(int i = 0; i < thousand; i++) {
                value += "M";
            }
        }
        
        // Handle the hundreds digit
        if(hundred != 0) {
            switch(hundred) {
                case 1: ;
                case 2: ;
                case 3: ;
                    // Add any needed 'C's
                    for(int i = 1; i <= hundred; i++) {
                        value += "C";
                    }
                break;
                case 4: 
                    value += "CD";
                break;
                case 5:
                case 6:
                case 7:
                case 8:
                    value += "D";
                    // Add any needed 'C's
                    for(int i = 5; i < hundred; i++) {
                        value += "C";
                    }
               break;
               case 9:
                    value += "CM";
               break;
               default:
            }
        }
        
        // Handle the tens digit
        if(ten != 0) {
            switch(ten) {
                case 1: ;
                case 2: ;
                case 3: ;
                    // Add any needed 'X's
                    for(int i = 1; i <= ten; i++) {
                        value += "X";
                    }
                break;
                case 4: 
                    value += "XL";
                break;
                case 5:
                case 6:
                case 7:
                case 8:
                    value += "L";
                    // Add any needed 'X's
                    for(int i = 5; i < ten; i++) {
                        value += "X";
                    }
               break;
               case 9:
                    value += "XC";
               break;
               default:
            }
        }
        
        // Handle the ones digit
        if(one != 0) {
            switch(one) {
                case 1: ;
                case 2: ;
                case 3: ;
                    // Add any needed 'I's
                    for(int i = 1; i <= one; i++) {
                        value += "I";
                    }
                break;
                case 4: 
                    value += "IV";
                break;
                case 5:
                case 6:
                case 7:
                case 8:
                    value += "V";
                    // Add any needed 'I's
                    for(int i = 5; i < one; i++) {
                        value += "I";
                    }
               break;
               case 9:
                    value += "IX";
               break;
               default:
            }
        }
        
        return value;
    }
}
