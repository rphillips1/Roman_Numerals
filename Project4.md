### *CS-140 BL --- Summer I 2016*

# Project 4 - Roman Numerals

## Objectives
The objective of this project is to have students use remainder (`%`), conditionals (`if-else`), and keyboard input using the `Scanner` class.  If you want, you can use the `switch` statement. 

## Specifications
In this project, you create two classes:
* `Conversion.java` – has one `static` method `convert()` that takes an `integer`, and returns a `String` (which is the Roman numeral representation of the number).  This class has no instance variables. This method has a whole bunch of `if` statements.  It is not difficult once you get the hang of the conversion process.   * Project4.java:	* Has one method `main`
	* In the `main`, you prompt the user for an integer in the range 1-3999.  If the user entered a value outside this range, you print out a message and quit the program.
	* If the user entered an integer in the given range, you call the method `convert` as follows:		`Conversion.convert(…)` – the string that is returned by this method has to be printed on the screen.  Recall that, `static` methods are typically called with the class name not with an object reference.
  Write a program that converts a positive integer into the Roman number system.  The Roman number system has digits:

Roman Numeral | Arabic Number
:-: | :-:I |	1V | 5X | 10L | 50C | 100D | 500M | 1,000Numbers are formed according to the following rules:1. Only numbers up to 3,999 are represented.2. As in the decimal system, the thousands, hundreds, tens and ones are expressed separately.3. The numbers 1 to 9 are expressed as:

	Roman Numerals | Arabic Number
	:-: | :-:	I | 1	II | 2		III	| 3	
	IV | 4
	V | 5
	VI | 6
	VII | 7
	VIII | 8
	IX | 9
		As you can see, an I preceding a V or X is subtracted from the value, and you can never have more than three I’s in a row.4. Tens and hundreds are done the same way, except that the letters X, L, C and C, D, M are used instead of I, V, X respectively.Your program should take an input, such as 1978, and convert it to Roman numerals, MCMLXXVIII.Several runs of the program are given in Output.txt 
### This is an Individual Assignment - No Partners
As this is a Project (and not a Lab) you will be working on your own, not with a partner. You should not be sharing your code with anyone else, other than the instructor.  

You will need to fork your own private Project4 repository on GitLab for this project. The only person who should have any access to your repository is your instructor.  

You can ask questions on Piazza about setting up your repository on GitLab, about using Git to send code to the instructor, and general questions about how to write your code. However you should not be posting sections of code and asking others to find your errors. 

### Deliverables
Be sure that you have your name and an explanation of what your program and every method does in the Javadoc comments. Be sure that you have indented consistently.  
 
The instructor will pull your Project4 from your GitLab repository to grade it. Make sure:
 
1. You have pushed all changes to your shared repository. (I can’t access local changes on your computer.)  
2. You have added your instructor as Master to your shared GitLab repository.  

### Due Date/Time
Your project must be pushed to GitLab by Tuesday, 21 June 2016 at 11:59pm. 
	
##Copyright and License
####&copy;2016 Karl R. Wurst and Aparna Mahadev, Worcester State University

<img src="http://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-sa.png" width=100px/>This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License. To view a copy of this license, visit <a href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank">http://creativecommons.org/licenses/by-sa/4.0/</a> or send a letter to Creative Commons, 444 Castro Street, Suite 900, Mountain View, California, 94041, USA.