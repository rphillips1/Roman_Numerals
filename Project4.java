import java.util.Scanner;

/**
 * Write a description of class Project4 here.
 * 
 * @author Rick W. Phillips
 * @version June 16, 2016
 */
public class Project4
{
    public static void main(String[] args) {
        // Welcome text
        System.out.println("Welcome to the Roman Numeral Converter V0.1 by Rick W. Phillips\n\n");
        // Display sentinel condition
        System.out.print("Please, enter a year between 1-3999(Enter anything else to quit): ");
        Scanner in = new Scanner(System.in);
        String input = in.next();
        int year = input.matches("\\d+") ? Integer.parseInt(input) : 0; // Validate input
        
        while(year >= 1 && year <= 3999) { // Check sentinel condition
            // Per our instructions, create the Conversion class with a 
            // static .convert(int) method that returns a String to be printed
            System.out.println(Conversion.convert(year));
            
            // Display sentinel condition
            System.out.print("Please, enter a year between 1-3999(Enter anything else to quit): ");
            input = in.next();
            year = input.matches("\\d{1,4}") ? Integer.parseInt(input) : 0;
        }
        
        System.out.print("\n\n\n\n\nThank you for using the Roman Numeral Converter V0.1 by Rick W. Phillips");
    }
    
    /**
     * Test method to view all the years and Roman numerals. Prints all years to output.
     */
    public static void testYears() {
        for(int i = 1; i <= 3999; i++) {
            System.out.println("Year: " + i + " " + Conversion.convert(i));
        }
    }
}
