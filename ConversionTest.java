

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class ConversionTest.
 *
 * @author  Rick W. Phillips
 * @version June 16, 2016
 */
public class ConversionTest
{
    private String value;
    private int year;
    /**
     * Default constructor for test class ConversionTest
     */
    public ConversionTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    /**
     * Test four digit year 
     */
    @Test
    public void testFourDigitYear()
    {
        year = 1984;
        assertEquals("MCMLXXXIV", Conversion.convert(year));
    }
    
    /**
     * Test three digit year 
     */
    @Test
    public void testThreeDigitYear()
    {
        year = 744;
        assertEquals("DCCXLIV", Conversion.convert(year));
    }
    
    /**
     * Test two digit year 
     */
    @Test
    public void testTwoDigitYear()
    {
        year = 98;
        assertEquals("XCVIII", Conversion.convert(year));
    }
    
    /**
     * Test one digit year 
     */
    @Test
    public void testOneDigitYear()
    {
        year = 3;
        assertEquals("III", Conversion.convert(year));
    }
}
